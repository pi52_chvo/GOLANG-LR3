package main

import (
	"fmt"
	"math"
)

func main() {
	numberCount := 3000
	x := int64(1)
	var array [3000]float64
	for i := 0; i < numberCount; i++ {
		array[i] = Con(&x)
	}
	mMap := GetP(array)
	for k := range mMap {
		fmt.Println(k, " - ", mMap[k])
	}
	spod := MathSpod(mMap)
	fmt.Println("Математичне сподівання : ", spod)
	disp := Disp(mMap, spod)
	fmt.Println("Дисперсія : ", disp)
	fmt.Println("Середньоквадратичне відхилення : ", math.Sqrt(disp))
}

func Con(x0 *int64) float64 {
	m := int64(math.Pow(2, 32))
	a := int64(65539)
	c := int64(0)
	res := float64(0)
	for {
		*(x0) = ((a * *(x0)) + c) % m
		res = float64(*(x0)) / float64(m) * 1000
		if res <= 200 {
			break
		}
	}
	return toFixed(res, 2)
}

func IsInMap(mass map[float64]float64, key float64) bool {
	if mass[key] == 0 {
		return false
	}
	return true
}

func GetNumCountInArray(array [3000]float64, num float64) int64 {
	count := 0
	for i := 0; i < len(array); i++ {
		if array[i] == num {
			count++
		}
	}
	return int64(count)
}

func GetP(array [3000]float64) map[float64]float64 {
	myMap := make(map[float64]float64)
	for i := 0; i < len(array); i++ {
		if !IsInMap(myMap, array[i]) {
			myMap[array[i]] = float64(GetNumCountInArray(array, array[i])) / 3000.0
		}
	}
	return myMap
}

func MathSpod(myMap map[float64]float64) float64 {
	var spod float64 = 0
	for k := range myMap {
		spod = spod + myMap[k]*float64(k)
	}
	return spod
}
func MathSpodSquare(myMap map[float64]float64) float64 {
	var spod float64 = 0
	for k := range myMap {
		spod = spod + myMap[k]*float64(k)*float64(k)
	}
	return spod
}

func Disp(myMap map[float64]float64, mathSpod float64) float64 {
	var disp float64 = 0
	spodSquare := MathSpodSquare(myMap)
	disp = spodSquare - mathSpod*mathSpod
	return disp
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}
